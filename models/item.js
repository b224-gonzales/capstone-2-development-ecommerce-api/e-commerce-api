const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const Item = mongoose.model('Item', userSchema);


const itemSchema = new mongoose.Schema({
    owner : {
       type: ObjectID,
       required: true,
       ref: 'User'
    },
    name: {
       type: String,
       required: true,
       trim: true
    },
    description: {
      type: String,
      required: true
    },
    category: {
       type: String,
       required: true
    },
    price: {
       type: Number,
       required: true
    }
    }, {
    timestamps: true
    });





module.exports = Item;
